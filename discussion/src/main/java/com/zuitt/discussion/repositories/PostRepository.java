package com.zuitt.discussion.repositories;

import com.zuitt.discussion.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

//An interface with @Repository will contain methods for database manipulation;
@Repository
public interface PostRepository extends CrudRepository<Post,Object> {
//Post is the datatype of the repository
//Object is the datatype of the item returned from the db

    List<Post> findByContent(String content);

}
